"""This module provides an interface to build a CLI for a program
from Python's standard argparse module.
"""

import argparse
import json
import os
import os.path


_SPECIAL_TYPES = {}


class EasyCLIError(Exception):
    """Base error for this module."""


class BuildError(EasyCLIError):
    """Indicates a problem adding argument/mode."""


def add_special_type(label, func):
    """Add a special type (callable) to CLI.
    This is accessible by the string label when adding arguments to the
    parser via 'special_type' field.
    """
    _SPECIAL_TYPES[label] = func


def build_parser_from_json_file(filename):
    """See build_parser function.
    Wraps that in a read from JSON file.
    File must be a JSON object (dictionary) with appropriate key/values.
    """
    with open(filename, 'r') as json_f:
        try:
            return build_parser(**json.load(json_f))
        except json.JSONDecodeError as err:
            raise EasyCLIError('Invalid JSON in {}: {}'.format(filename, err))


def build_parser(prog_name=None,
                 prog_desc=None,
                 modes=None,
                 arguments=None,
                 **kwargs):
    """Build and return an argparse.ArgumentParser object based on
    specification.
    """
    parser = argparse.ArgumentParser(
        prog=prog_name,
        description=prog_desc)
    # Build out the root 'mode' like subordinate modes
    subparsers_dict = {'root': {'parser': parser}}
    options_dict = {}
    _assign_options(options_dict, 'root', **kwargs)

    for mode in modes if modes else []:
        # Build out subordinate modes
        _add_mode(subparsers_dict, options_dict, **mode)
    for argument in arguments if arguments else []:
        _add_argument(subparsers_dict, **argument)

    return parser


# Special callables for this module
# Provide useful argument types for a CLI
def input_file(filename):
    """Ensures an input file exists and is readable."""
    path = os.path.realpath(filename)
    if not os.path.isfile(path):
        raise argparse.ArgumentTypeError('{} does not exist'.format(path))
    if not os.access(path, os.R_OK):
        raise argparse.ArgumentTypeError('{} is not readable'.format(path))
    return path


def input_json_file(filename):
    """Ensures a JSON input file exists, then attempts to read and load it.
    """
    path = input_file(filename)
    with open(path, 'r') as json_f:
        try:
            return json.load(json_f)
        except json.JSONDecodeError as err:
            raise argparse.ArgumentTypeError(
                'Invalid JSON in {}: {}'.format(path, err))


def output_file(filename):
    """Ensures an output file is readable. Could exist or could not exist."""
    path = os.path.realpath(filename)
    if os.path.isdir(path):
        raise argparse.ArgumentTypeError('{} is a directory'.format(path))
    dirpath = os.path.dirname(path)
    if not os.path.isdir(dirpath):
        raise argparse.ArgumentTypeError(
            '{} directory does not exist'.format(dirpath))
    if not os.access(dirpath, os.W_OK|os.X_OK):
        raise argparse.ArgumentTypeError('{} is not writeable'.format(path))
    return path


def int_st(int_string):
    """Int callable"""
    try:
        return int(int_string)
    except ValueError:
        raise argparse.ArgumentTypeError(
            'Invalid integer: {}'.format(int_string))


def float_st(float_string):
    """Float callable"""
    try:
        return float(float_string)
    except ValueError:
        raise argparse.ArgumentTypeError(
            'Invalid floating point number: {}'.format(float_string))


def _add_mode(subparsers_dict, options_dict, parent, name, **kwargs):
    """Add a mode (subparser) to the root parser."""
    # Parse, implement, and remove mode-sepcific flags
    _assign_options(options_dict, name, **kwargs)

    # Extract parent parser configuration
    parent_dict = subparsers_dict.get(parent, None)
    parent_options_dict = options_dict[parent]
    if not parent_dict:
        raise BuildError('Parser named {} not found'.format(parent))
    parent_subparsers = parent_dict.get('subparsers', None)
    if not parent_subparsers:
        # Setup the parent's modes if not yet setup
        parent_dict['subparsers'] = parent_dict['parser'].add_subparsers(
            dest=parent_options_dict['mode_dest'],
            help=parent_options_dict['mode_help'],
            required=parent_options_dict['mode_required'])
        parent_subparsers = parent_dict['subparsers']
    # Setup this specific mode
    subparsers_dict[name] = {'parser': parent_subparsers.add_parser(
        name, **kwargs)}


def _add_argument(subparsers_dict, parents, flags, special_type=None, **kwargs):
    try:
        parent_parsers = [subparsers_dict[parent]['parser'] for
                          parent in parents]
    except KeyError:
        raise BuildError('Not all parsers in {} found'.format(parents))
    if special_type:
        try:
            kwargs['type'] = _SPECIAL_TYPES[special_type]
        except KeyError:
            raise BuildError(
                'Special type [{}] not recognized'.format(special_type))
    for parent in parent_parsers:
        parent.add_argument(*flags, **kwargs)


def _assign_options(options_dict, name, **kwargs):
    """Extract specific flags from kwargs which indicate mode options
    for this parser.
    """
    option_flags = ['mode_dest', 'mode_help', 'mode_required']
    option_defaults = {
        'mode_dest': 'mode',
        'mode_help': 'mode of operation',
        'mode_required': False,
    }
    options_dict[name] = {
        # Set the options for this parser based on special flags
        option_flag: kwargs.get(option_flag, option_defaults[option_flag]) for
        option_flag in option_flags}
    for flag in option_flags:
        # Clear special flags from kwargs which will be propagated to
        # argparse later
        if flag in kwargs.keys():
            del kwargs[flag]


_SPECIAL_TYPES = {
    'InputFile': input_file,
    'InputJSONFile': input_json_file,
    'OutputFile': output_file,
    'Int': int_st,
    'Float': float_st,
}


if __name__ == '__main__':
    pass

"""Evaluate functionality of CLI module."""

import argparse
import json
import os
import tempfile
import unittest

import cli


class TestCLITypes(unittest.TestCase):
    """Evaluate special input types available in CLI module."""
    FAKE_DIR = 'fakedir23232'
    FAKE_FILENAME = 'fake!!!!%%%%'

    def test_input_file(self):
        fd, filename = tempfile.mkstemp(suffix='.txt', text=True)
        os.close(fd)
        # Should return without raising an error
        filepath = cli.input_file(filename)
        os.remove(filepath)

        # Should error because doesn't exist
        with self.assertRaises(argparse.ArgumentTypeError):
            filepath = cli.input_file(self.FAKE_FILENAME)

    def test_input_json_file(self):
        fd, filename = tempfile.mkstemp(suffix='.json', text=True)
        os.close(fd)
        test_dict = {'hello': 'world'}
        with open(filename, 'w') as json_f:
            json.dump(test_dict, json_f)

        self.assertEqual(test_dict, cli.input_json_file(filename))
        os.remove(filename)

    def test_output_file(self):
        # Should allow not-existing file
        filepath = cli.output_file(self.FAKE_FILENAME)

        # Should error because lacks write permissions
        with self.assertRaises(argparse.ArgumentTypeError):
            filepath = cli.output_file(
                os.path.join('/', 'etc', self.FAKE_FILENAME))

        # Should error because not directory
        with self.assertRaises(argparse.ArgumentTypeError):
            filepath = cli.output_file(
                os.path.join(self.FAKE_DIR, self.FAKE_FILENAME))

        # Should error because is directory
        with self.assertRaises(argparse.ArgumentTypeError):
            filepath = cli.output_file('tests')


if __name__ == '__main__':
    unittest.main()

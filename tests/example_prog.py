#!/usr/bin/env python3
"""Toy program to demonstrate the CLI module."""

import sys
sys.path.append('.')
sys.path.append('..')

import cli


MODES = [
    {
        'parent': 'root',
        'name': 'fastmode',
        'help': 'Execute very quickly',
    },
]
ARGUMENTS = [
    {
        'parents': ['fastmode'],
        'flags': ['-a', '--action'],
        'help': 'The action to take',
    }
]


def main():
    parser = cli.build_parser(modes=MODES,
                              arguments=ARGUMENTS,
                              mode_required=True)
    args = parser.parse_args()
    print(args)


if __name__ == '__main__':
    main()
